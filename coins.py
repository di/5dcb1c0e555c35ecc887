import cv2
import numpy as np

img = cv2.imread("coins.png", cv2.IMREAD_GRAYSCALE)

circles = cv2.HoughCircles(
    img,
    cv2.cv.CV_HOUGH_GRADIENT,
    dp=1.5,
    minDist=30,
    minRadius=15,
    maxRadius=60,
)

black = np.zeros(img.shape)
for x, y, r in circles[0]:
    # -1 to draw filled circles
    cv2.circle(black, (x,y), int(r+15), 255, -1)

bytemask = np.asarray(black, dtype=np.uint8)
inpainted = cv2.inpaint(img, bytemask, inpaintRadius=5, flags=cv2.INPAINT_TELEA)

cv2.imshow('image',inpainted)
cv2.waitKey(0)
cv2.destroyAllWindows()